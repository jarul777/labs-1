﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Impl3 : Contract.IPolicjant, Contract.IRobot
    {
        public string Przyjdź()
        {
            return "Przybywam robocić!";
        }

        public string Nakrzycz()
        {
            return "Krzyczę jak R2D2!";
        }

        public string Upomnij()
        {
            return "Roboty nie lubią jak się je polewa!";
        }

        public string Działaj()
        {
            return "Już robię z tym porządek";
        }

        string Contract.IMundurowy.Nakrzycz()
        {
            return "Krzyczę jak mundurowy!";
        }

        string Contract.IPolicjant.Nakrzycz()
        {
            return "Krzyczę jak policjant!";
        }

        string Contract.IRobot.Nakrzycz()
        {
            return "Krzyczę jak robot!";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(Contract.IMundurowy);
        
        public static Type ISub1 = typeof(Contract.IPolicjant);
        public static Type Impl1 = typeof(Implementation.Impl1);
        
        public static Type ISub2 = typeof(Contract.IStrazak);
        public static Type Impl2 = typeof(Implementation.Impl2);
        
        
        public static string baseMethod = "Nakrzycz";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Przyjdź";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Przyjdź";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Kolekcja";
        public static string collectionConsumerMethod = "Operacja";

        #endregion

        #region P3

        public static Type IOther = typeof(Contract.IRobot);
        public static Type Impl3 = typeof(Implementation.Impl3);

        public static string otherCommonMethod = "Nakrzycz";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        public IList<IMundurowy> Kolekcja()
        {
            IList<IMundurowy> lista = new List<IMundurowy>();

            lista.Add(new Impl1());
            lista.Add(new Impl2());
            lista.Add(new Impl2());
            lista.Add(new Impl3());

            lista[0].Nakrzycz();
            lista[1].Nakrzycz();
            lista[2].Nakrzycz();
            lista[3].Nakrzycz();

            return lista;
        }

        public void Operacja(IList<IMundurowy> lista)
        {
            foreach (var m in lista)
            {
                m.Nakrzycz();
            }
        }

        static void Main(string[] args)
        {
            
        }
    }
}

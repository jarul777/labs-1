﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    public interface IStrazak : IMundurowy
    {
        new string Nakrzycz();
        new string Przyjdź();
    }
}
